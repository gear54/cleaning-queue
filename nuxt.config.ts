import { defineNuxtConfig } from 'nuxt3'

export default defineNuxtConfig({
  ssr: true,
  build: { transpile: ['date-fns', 'vue-use-infinite-scroll'] },
});
