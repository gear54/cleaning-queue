/**
 * Returns zones sorted by dirtiness (descending). This
 * means that the first zone in the returned array is the
 * one to be cleaned now barring any overrides.
 *
 * @param {string[]} zones Unordered zone names
 * @param {string[]} cleaned Which zones were cleaned (most recent last)
 * @return {string[]} Zones sorted by priority
 * 
 * @example
 * getNextZones(['a', 'b', 'c'], ['c', 'b']) // => ['a', 'c', 'b']
 */
function getNextZones(zones: string[], cleaned: string[]) {
  return zones.slice().sort((a, b) => cleaned.lastIndexOf(a) - cleaned.lastIndexOf(b));
}

export default getNextZones;
