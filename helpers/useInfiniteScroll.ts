import { Ref, ref, onMounted, onUnmounted } from 'vue';

function useInfiniteScroll(domElement: Ref<HTMLElement | null>) {
  const page = ref(1);
  let observer: IntersectionObserver | undefined = undefined;

  onMounted(() => {
    if (!domElement.value) {
      return;
    }

    observer = new IntersectionObserver(([entry]) => {
      if (entry && entry.isIntersecting) {
        page.value++;
      }
    });

    observer.observe(domElement.value)
  });

  onUnmounted(() => { observer?.disconnect(); });

  return page;
}

export default useInfiniteScroll;
