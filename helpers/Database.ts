import * as fs from 'fs';

import getNextZones from './getNextZones';

type Model = {
  zones: { name: string; title: string; iconUrl: string; }[];
  events: { zoneName: string; date: Date; }[];
};

const EVENT_PAGE_SIZE = 10;

class Database {
  private model: Model;

  constructor(private fileName: string = './db.json') {
    const parsed: {
      zones: { name: string; title: string; iconUrl: string; }[];
      events: { date: string; zoneName: string }[];
    } = JSON.parse(fs.readFileSync(this.fileName, 'utf-8'));

    this.model = {
      ...parsed,

      events: parsed.events.map(event => ({ ...event, date: new Date(event.date) })),
    };
  }

  private write() {
    const serialized = {
      ...this.model,

      events: this.model.events.map(event => ({ ...event, date: event.date.toISOString() })),
    };

    fs.writeFileSync(this.fileName, JSON.stringify(serialized, undefined, 2), 'utf-8');
  }

  getNextZones() {
    const { zones, events } = this.model;

    const nextZoneNames = getNextZones(zones.map(({ name }) => name), events.map(({ zoneName }) => zoneName))

    return nextZoneNames.map(name => {
      const zone = zones.find(zone => zone.name === name);

      // This shouldn't ever trigger, zones that are named in events should always be in zones data
      if (!zone) {
        throw new Error(`No such zone: ${name}`);
      }

      return zone;
    });
  }

  getEvents(page: number) {
    const pageStart = page * EVENT_PAGE_SIZE;
    const pageEnd = pageStart + EVENT_PAGE_SIZE;

    return {
      events: this.model.events.reverse().slice(pageStart, pageEnd),
      hasNextPage: this.model.events.length >= pageEnd + 1,
    };
  }

  addEvent(zoneName: string, date: Date) {
    const { zones } = this.model;

    if (!zones.some(({ name }) => name === zoneName)) {
      throw new Error(`No such zone: ${zoneName}`);
    }

    const newEvent = { zoneName, date };

    this.model.events.push(newEvent);
    this.write();

    return newEvent;
  }
}

export default Database;
