import { Ref } from 'vue';

function useAction<Args extends any[]>(action: (...args: Args) => Promise<void>, isLoading: Ref<boolean>, errorMessage: Ref<string | undefined>) {
  return async (...args: Args) => {
    isLoading.value = true;
    errorMessage.value = undefined;

    try {
      await action(...args);
    } catch (error) {
      errorMessage.value = error.message;
    }

    isLoading.value = false;
  }
}

export default useAction;
