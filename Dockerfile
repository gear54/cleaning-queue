FROM node:16-alpine

USER node:node
WORKDIR /home/node/app
EXPOSE 3000

COPY --chown=node:node .output ./.output
COPY --chown=node:node package.json .

ENV NODE_ENV=production
ENV NUXT_HOST=0.0.0.0
ENTRYPOINT [ "yarn", "start" ]
