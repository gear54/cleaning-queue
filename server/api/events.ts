import { IncomingMessage, ServerResponse } from 'http';

import { useQuery } from 'h3';

import Database from '~/helpers/Database';

export default async (req: IncomingMessage) => {
  const db = new Database();
  const query = useQuery(req);
  const page = Number.parseInt(String(query.page), 10);

  if (!(page >= 0)) {
    throw new Error('No page number supplied');
  }

  const { events, hasNextPage } = db.getEvents(page);

  return {
    events: events.map(event => ({ ...event, date: event.date.toISOString() })),
    hasNextPage,
  };
}
