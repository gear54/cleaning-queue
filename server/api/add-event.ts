import { IncomingMessage, ServerResponse } from 'http';

import { useBody, sendError, createError } from 'h3';

import Database from '~/helpers/Database';

export default async (req: IncomingMessage, res: ServerResponse) => {
  const db = new Database();

  if (req.method === 'POST') {
    const { zoneName } = await useBody<{ zoneName: string; }>(req);

    if (!zoneName) {
      throw new Error('No \'zoneName\' supplied');
    }

    const newEvent = db.addEvent(zoneName, new Date());

    return {
      newEvent: { ...newEvent, date: newEvent.date.toISOString() },
      nextZones: db.getNextZones(),
    };
  }

  sendError(res, createError({ statusCode: 404 }));
}
