import Database from '~/helpers/Database';

export default async () => {
  const db = new Database();

  return db.getNextZones();
}
